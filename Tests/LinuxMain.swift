import XCTest

import VoIPKitTests

var tests = [XCTestCaseEntry]()
tests += VoIPKitTests.allTests()
XCTMain(tests)
