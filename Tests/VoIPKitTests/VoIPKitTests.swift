import XCTest
@testable import VoIPKit

final class VoIPKitTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(VoIPKit().text, "Hello, World!")
    }
    
    func test_CallManager_StartCall() {
        let expectation = XCTestExpectation()
        
        let callManager = CallManager.shared
        let call = callManager.startCall()
        
        let target = call.state.getString()
        
        XCTAssertEqual(target, "VoIPKit.StartingState", "[Failed] The call state is \(target)")
        expectation.fulfill()
        
        self.wait(for: [expectation], timeout: 15)
    }
    
    func test_CallManager_didReceiveStartCall() {
        let expectation = XCTestExpectation()
        
        let callUUID = UUID()
        let event = Event(callId: callUUID.uuidString, type: .start)
        let callManager = CallManager.shared
        callManager.didReceiveEvent(event)
        guard let call = callManager.call(forUUID: callUUID) else {
            XCTFail("[Failed] Call for \(callUUID.uuidString) doesn't exist")
            return
        }
        let target = call.state.getString()
        
        XCTAssertEqual(target, "VoIPKit.IdleState", "[Failed] The call state is \(target)")
        expectation.fulfill()
        
        self.wait(for: [expectation], timeout: 15)
    }
    
    func test_DirectCall_acceptCall() {
        let expectation = XCTestExpectation()
        
        let callUUID = UUID()
        let event = Event(callId: callUUID.uuidString, type: .start)
        let callManager = CallManager.shared
        callManager.didReceiveEvent(event)
        guard let call = callManager.call(forUUID: callUUID) else {
            XCTFail("[Failed] Call for \(callUUID.uuidString) doesn't exist")
            return
        }
        
        call.accept()
        
        let target = call.state.getString()
        
        XCTAssertEqual(target, "VoIPKit.AcceptingState", "[Failed] The call state is \(target)")
        expectation.fulfill()
        
        self.wait(for: [expectation], timeout: 15)
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}

extension CallState {
    /**
     Get `String` value of `State` object.
     
     - Returns:
     `"V2oIP.\(className)"`
     
     - note:
     ```Swift
     IdleState().getString()    // V2oIP.IdleState
     ```
     */
    func getString() -> String {
        return String(describing: self)
    }
}
