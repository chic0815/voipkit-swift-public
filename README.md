# VoIPKit

This VoIP Kit provide `DirectCall` object and `CallManager` object with basic `CallStates`

You can implement your own call process but this Swift Package will be more helpful.

You have to use this package with `CallKit` and `WebRTC`

## Copyright

© 2019 Jaesung. All Rights Reserved.

## Contact

**Jaesung**

chic0815@icloud.com

## Supporting

> **Supported Platform**: iOS 10.0 or later

> **Versions of Swift**: Swift 5.0 or later

## How to use

### How to import Swift Package Manager
> **URL**: git@gitlab.com:chic0815/voip-state-machine-swift-public.git

Go to Project > Swift Package

Press + button

Enter package repository URL (git@gitlab.com:chic0815/voip-state-machine-swift-public.git)

Select Version and check the version(You can check current version on "Version History")

Go to file in where you want to import `VoIPKit`

`import VoIPKit`


### How to start call

Please use `CallManager.startCall()` method.  This method returns `DirectCall` object with its UUID called `callUUID`

Please copy the call's UUID to some property.

You can access the call with `CallManager.call(forUUID:)` method.  This method returns `DirectCall` object with the `UUID` you copied before.



### How to send specific request from call to my server

Please use `CallRequestable` protocol

- `requestStart(call:completionHander:)`

If server responds with error, call will be ended.
You need to allocate `caller` and `callee` information to `DirectCall.caller`, `DirectCall.callee`

- `requestAccept(call:completionHander:)`

If server responds with error, call will be ended.

- `requestOffer(call:completionHander:)`

If server responds with error, call will be ended.

- `requestAnswer(call:completionHander:)`

If server responds with error, call will be ended.

- `requestCancel(call:)`

- `requestDecline(call:)`

- `requestEnd(call:)`

- `requestTimeout(call:)`


### How to change my UI with changes of call state

Please use `CallUIImplementable` protocol.

```Swift
class CallViewController: UIViewController, CallUIImplementable {

    // ...
    
    func didStart(call: DirectCall) {
        // ...
    }
    
    func didReceive(call: DirectCall) {
        // ...
    }
    
    func didConnect(call: DirectCall) {
        // ...
    }
    
    func didReceiveAudioConfiguration(of call: DirectCall) {
        // ...
    }
    
    func didEnd(call: DirectCall) {
        // ...
    }
}
```

### How to connect to `WebRTC`

You need to call `DirectCall.didWebRTCConnect()` inside of `WebRTC.didConnect()`


## Version History
1.0.2

> Dec 7, 2019

1.0.1

> Dec 7, 2019

1.0.0

> Dec 7, 2019
