//
//  File.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

public class User {
    public let id: String
    public var nickname: String?
    
    public init(id: String) {
        self.id = id
    }
}
