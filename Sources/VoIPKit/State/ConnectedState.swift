//
//  ConnectedState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

class ConnectedState: CallState {
    func didEndCall(_ call: DirectCall) {
        call.changeState(to: EndingState(call: call))
    }
    
    func didReceiveEnd(of call: DirectCall) {
        call.changeState(to: EndingState(call: call))
    }
}
