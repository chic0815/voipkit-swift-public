//
//  EndingState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

class EndingState: CallState {
    init(call: DirectCall) {
        call.uiImplementer?.didEnd(call: call)
    }
}
