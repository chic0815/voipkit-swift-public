//
//  CallState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

/**
 This protocol will control methods in DirectCall
 */
protocol CallState {
    // Caller
    func didStartCall(_ call: DirectCall)
    
    func didCancelCall(_ call: DirectCall)
    
    func didReceiveAccept(of call: DirectCall)
    
    func didReceiveAnswer(of call: DirectCall)
    
    // Callee
    func didReceiveStart(of call: DirectCall)
    
    func didAcceptCall(_ call: DirectCall)
    
    func didDeclineCall(_ call: DirectCall)
    
    func didReceiveOffer(of call: DirectCall)
    
    // Common
    func didConnectCall(_ call: DirectCall)
        
    func didEndCall(_ call: DirectCall)
    
    func didReceiveEnd(of call: DirectCall)
    
    func didTimeoutCall(_ call: DirectCall)
}

extension CallState {
    // Caller
    func didStartCall(_ call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didCancelCall(_ call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didReceiveAccept(of call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didReceiveAnswer(of call: DirectCall) {
        print("[State] \(#function)")
    }
    
    
    // Callee
    func didReceiveStart(of call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didAcceptCall(_ call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didDeclineCall(_ call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didReceiveOffer(of call: DirectCall) {
        print("[State] \(#function)")
    }
    
    
    // Common
    func didConnectCall(_ call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didEndCall(_ call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didReceiveEnd(of call: DirectCall) {
        print("[State] \(#function)")
    }
    
    func didTimeoutCall(_ call: DirectCall) {
        print("[State] \(#function)")
    }
}
