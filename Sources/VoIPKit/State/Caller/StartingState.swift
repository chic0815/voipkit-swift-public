//
//  StartingState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

class StartingState: CallState {
    func didReceiveAccept(of call: DirectCall) {
        call.offer()
        call.changeState(to: OfferingState())
    }
    
    func didEndCall(_ call: DirectCall) {
        call.cancel()
    }
    
    func didCancelCall(_ call: DirectCall) {
        call.changeState(to: EndingState(call: call))
    }
}
