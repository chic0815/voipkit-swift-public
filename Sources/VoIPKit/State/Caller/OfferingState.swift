//
//  OfferingState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

class OfferingState: CallState {
    
    func didConnectCall(_ call: DirectCall) {
        call.changeState(to: ConnectedState())
    }
}
