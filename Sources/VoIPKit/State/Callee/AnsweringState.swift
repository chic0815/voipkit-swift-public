//
//  AnsweringState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

class AnsweringState: CallState {
    func didConnectCall(_ call: DirectCall) {
        call.changeState(to: ConnectedState())
    }
}
