//
//  AcceptingState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

class AcceptingState: CallState {
    // MARK: Positive
    func didReceiveOffer(of call: DirectCall) {
        call.answer()
        call.changeState(to: AnsweringState())
    }
    
    
    // MARK: Negative
    func didEndCall(_ call: DirectCall) {
        call.changeState(to: EndingState(call: call))
    }
    
    func didReceiveEnd(of call: DirectCall) {
        call.changeState(to: EndingState(call: call))
    }
}
