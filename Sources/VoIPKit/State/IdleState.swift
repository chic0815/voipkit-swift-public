//
//  IdleState.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

class IdleState: CallState {
    
    // Caller
    func didStartCall(_ call: DirectCall) {
        call.changeState(to: StartingState())
        call.requestStartCall()
    }
    
    
    // Callee
    func didAcceptCall(_ call: DirectCall) {
        call.changeState(to: AcceptingState())
        call.requestAcceptCall()
    }
    
    func didDeclineCall(_ call: DirectCall) {
        call.changeState(to: EndingState(call: call))
    }
    
    func didEndCall(_ call: DirectCall) {
        call.decline()
    }
}
