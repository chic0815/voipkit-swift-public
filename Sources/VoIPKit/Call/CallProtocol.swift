//
//  File.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

import Foundation

public typealias CompletionHandler = (_ error: Error?) -> Void

/**
 Help implement call UI with call state.
 
 - Since: 1.0.0
 */
public protocol CallUIImplementable {
    /**
     When the caller started a new call.
     */
    func didStart(call: DirectCall)
    
    /**
     When the callee receive a new call.
     */
    func didReceive(call: DirectCall)
    
    /**
     When the caller and the callee connected.
     */
    func didConnect(call: DirectCall)
    
    /**
     When someone received change of audio configuration from participant.
     
     - Cases: Mute / Unmute
     */
    func didReceiveAudioConfiguration(of call: DirectCall)
    
    /**
     When the call has been ended.
     */
    func didEnd(call: DirectCall)
}

public protocol CallRequestable {
    func requestStart(call: DirectCall, completionHandler: CompletionHandler)
    
    func requestAccept(call: DirectCall, completionHandler: CompletionHandler)
    
    func requestCancel(call: DirectCall)
    
    func requestDecline(call: DirectCall)
    
    func requestOffer(call: DirectCall, completionHandler: CompletionHandler?)
    
    func requestAnswer(call: DirectCall, completionHandler: CompletionHandler?)
    
    func requestEnd(call: DirectCall)
    
    func requestTimeout(call: DirectCall)
}
