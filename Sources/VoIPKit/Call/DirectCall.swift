//
//  File.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

import Foundation

/**
 Call class. The `DirectCall` object has `callUUID` as an unique key.
 
 - Since: 1.0.0
 */
public class DirectCall {
    /**
     Like Identification, this is an unique property that can distinguish from other calls.
     */
    public let callUUID: UUID
    
    /**
     Caller.  If you want your own user class, you must inherit `User` to the class as a super class.
     */
    public var caller: User?
    
    /**
     Callee. If you want your own user class, you must inherit `User` to the class as a super class.
     */
    public var callee: User?
    
    var state: CallState = IdleState()
    
    var uiImplementer: CallUIImplementable?
    
    var requester: CallRequestable?
    
    /**
     Set up `callUUID`
     */
    init() {
        self.callUUID = UUID()
    }
    
    init(uuid: UUID) {
        self.callUUID = uuid
    }
    
    func changeState(to nextState: CallState) {
        print("[DirectCall]\(#function) \(state) -> \(nextState)")
        self.state = nextState
    }
}

extension DirectCall {
    func start() {
        self.state.didStartCall(self)
        self.uiImplementer?.didStart(call: self)
    }
    
    func requestStartCall() {
        print("[Request] \(#function)")
        self.requester?.requestStart(call: self, completionHandler: { error in
            guard error != nil else { return }
            self.state.didEndCall(self)
        })
    }
    
    func didReceiveStart() {
        self.state.didReceiveStart(of: self)
        self.uiImplementer?.didReceive(call: self)
    }
    
    func cancel() {
        self.state.didCancelCall(self)
        self.requester?.requestCancel(call: self)
    }
    
    func didReceiveCancel() {
        self.state.didReceiveEnd(of: self)
        self.requester?.requestCancel(call: self)
    }
    
    /**
     When the callee accept received new call.
     
     - Example:
     ```Swift
    call.accept()
     ```
     */
    public func accept() {
        print("[Debug] \(#function)")
        self.state.didAcceptCall(self)
    }
    
    func requestAcceptCall() {
        print("[Request] \(#function)")
        self.requester?.requestAccept(call: self, completionHandler: { error in
            guard error != nil else { return }
            self.state.didEndCall(self)
        })
    }
    
    func didReceiveAccept() {
        self.state.didReceiveAccept(of: self)
    }
    
    func decline() {
        self.state.didDeclineCall(self)
        self.requester?.requestDecline(call: self)
    }
    
    func didReceiveDecline() {
        self.state.didReceiveEnd(of: self)
    }
    
    func offer() {
        self.requester?.requestOffer(call: self, completionHandler: { error in
            guard error != nil else { return }
            self.state.didEndCall(self)
        })
    }
    
    func didReceiveOffer() {
        self.state.didReceiveOffer(of: self)
    }
    
    func answer() {
        self.requester?.requestAnswer(call: self, completionHandler: { error in
            guard error != nil else { return }
            self.state.didEndCall(self)
        })
    }
    
    func didReceiveAnswer() {
        self.state.didReceiveAnswer(of: self)
    }
    /**
     Call this method inside of `WebRTC.didConnect`
     
     - Example:
     ```Swift
     func didConnect() {
        call.didWebRTCConnect()
     }
     ```
     */
    public func didWebRTCConnect() {
        print("[Debug] \(#function)")
        self.state.didConnectCall(self)
        self.uiImplementer?.didConnect(call: self)
    }
    
    
    /**
     decline, cancel, end
     
     - Important: Do **not** implement UI inside of this method.
     */
    public func end() {
        print("[Debug] \(#function)")
        self.requester?.requestEnd(call: self)
        self.state.didEndCall(self)
    }
    
    func didReceiveEnd() {
        self.state.didReceiveEnd(of: self)
    }
    
    func didReceiveTimeout() {
        self.requester?.requestTimeout(call: self)
        self.state.didTimeoutCall(self)
    }
    
    func didReceiveAudioConfiguration() {
        self.uiImplementer?.didReceiveAudioConfiguration(of: self)
    }
}
