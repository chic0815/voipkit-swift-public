//
//  CallManager.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

import Foundation

/**
 Receives event from server.
 
 - Since: 1.0.0
 */
public protocol EventReceivable {
    
    /**
     Calls appriate `DirectCall` method from the event.
     
     - Parameters:
        - event: `Event` object
     */
    func didReceiveEvent(_ event: Event)
}

/**
 [Singleton] CallManager preserves calls containing ended call.
 
 - Since: 1.0.0
 */
public class CallManager {
    public static let shared = CallManager()
    
    var calls: [String: DirectCall] = [:]
    
    private func add(call: DirectCall) {
        self.calls.updateValue(call, forKey: call.callUUID.uuidString)
    }
    
    private func createNewCall(with uuid: UUID) {
        let call = DirectCall(uuid: uuid)
        
        self.add(call: call)
        
        print("[Debug] \(#function) \(uuid.uuidString)")
    }
    /**
     Start call and add it to  `CallManager`.
     
     - Returns: `DirectCall` object. (`discardableResult`)
     */
    @discardableResult public func startCall() -> DirectCall {
        let call = DirectCall()
    
        self.add(call: call)
        call.start()
        
        print("[Debug] \(#function)")
        
        return call
    }
    
    /**
     Get `DirectCall` object from `CallManager` with its `UUID`.
     
     - Parameters
        - callUUID: `UUID` value that you know
     
     - Returns: `DirectCall` object or `nil`
     */
    public func call(forUUID callUUID: UUID) -> DirectCall? {
        return calls[callUUID.uuidString]
    }
}

extension CallManager: EventReceivable {
    public func didReceiveEvent(_ event: Event) {
        var call = DirectCall()
        
        if event.type == .start {
            self.createNewCall(with: event.callUUID)
        }
        
        guard let object = self.call(forUUID: event.callUUID) else {
            print("[CallManager] \(#function) Failed to find call with UUID from the event")
            return
        }
        
        call = object
        self.deliver(to: call, event: event)
    }
    
    func deliver(to call: DirectCall, event: Event) {
        switch event.type {
        case .start:
            call.didReceiveStart()
        case .cancel:
            call.didReceiveCancel()
        case .accept:
            call.didReceiveAccept()
        case .decline:
            call.didReceiveDecline()
        case .offer:
            call.didReceiveOffer()
        case .answer:
            call.didReceiveAnswer()
        case .end:
            call.didReceiveEnd()
        case .timeout:
            call.didReceiveTimeout()
        case .audio:
            call.didReceiveAudioConfiguration()
        }
    }
}

