//
//  File.swift
//  
//
//  Created by Jaesung on 2019/12/07.
//

import Foundation

/**
 You must inherit this class to your own event class as a super class.
 */
public class Event {
    public enum CommandType {
        case start
        case cancel
        case accept
        case decline
        case offer
        case answer
        case end
        case timeout
        case audio
    }
    
    public let callUUID: UUID
    public let type: CommandType
    
    public init(callId: String, type: CommandType) {
        self.type = type

        guard let uuid = UUID(uuidString: callId) else {
            print("Event has not been implemented")
            self.callUUID = UUID(uuidString: "Failed")!
            return
        }
        self.callUUID = uuid
    }
}
